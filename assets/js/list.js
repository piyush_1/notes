var firebaseConfig = {
  apiKey: 'AIzaSyAxBMxJhTWIwUSEbd4_PHiJ4-532GxShms',
  authDomain: 'notesapp00.firebaseapp.com',
  databaseURL: 'https://notesapp00-default-rtdb.firebaseio.com',
  projectId: 'notesapp00',
  storageBucket: 'notesapp00.appspot.com',
  messagingSenderId: '493685402083',
  appId: '1:493685402083:web:20b5811d44f769224098d6'
}
firebase.initializeApp(firebaseConfig)
var today = new Date()
var date =
  today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate()
var time =
  today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds()
console.log(date, time)
let addListNameBtn = document.getElementById('addListNameBtn')
let user = 'abhay'
showLists()

//Event Lister to Add List Items

addListNameBtn.addEventListener('click', function (e) {
  e.preventDefault()
  let listNameToAdd = document.getElementById('listNameToAdd')
  let listInStorage = localStorage.getItem('listInStorage')
  let listItemValue = listNameToAdd.value
  let warning = document.getElementById('invalid-list-warning')
  var regex = /[A-Za-z0-9]/g

  if (listInStorage == null) {
    listObj = []
  } else {
    listObj = JSON.parse(listInStorage)
  }

  if (regex.test(listItemValue)) {
    var newListItem = {
      id: create_UUID(),
      name: listItemValue,
      isCompleted: false
    }
    listObj.push(newListItem)
    localStorage.setItem('listInStorage', JSON.stringify(listObj))
    firebase
      .database()
      .ref('users/' + user + '/lists/' + newListItem.id)
      .set({
        uId: newListItem.id,
        data: newListItem.name,
        isCompleted: false,
        createdAt: date + ' ' + time
      })
  } else {
    warning.style.visibility = 'visible'
    setTimeout(warningremove, 5000)
  }

  showLists()
  listNameToAdd.value = ''
})
function warningremove () {
  let warning = document.getElementById('invalid-list-warning')
  warning.style.visibility = 'hidden'
}
// Function to Show List items

function showLists () {
  let listsInStorage = localStorage.getItem('listInStorage')
  let listElements = document.getElementById('lists')
  let html = ''

  if (listsInStorage == null) {
    listsObj = []
  } else {
    listsObj = JSON.parse(listsInStorage).reverse()
  }

  listsObj.forEach(function (index) {
    if (index.isCompleted) {
      var classCompleted = 'completed'
      var isChecked = 'checked'
    } else {
      classCompleted = ''
      isChecked = ''
    }

    html += `
    <li class="list-item ${classCompleted}">
          <input type="checkbox" id="${index.id}" class="form-check-input list-item-check" onclick="checkAddress(this, this.id)" ${isChecked}/>
          <span class="list-item-data">${index.name}</span>
          <button id="${index.id}" onclick="deleteListItem(this.id)" class="list-item-delete">
            <i class="far fa-trash-alt"></i>
          </button>
        </li>
           
    `
  })

  if (listsObj.length != 0) {
    listElements.innerHTML = html
  } else {
    listElements.innerHTML = `<p style="margin-right: 10px; text-align: center; font-size: 20px">Nothing to Show</p>`
  }
}

//function to Delete item

function deleteListItem (index) {
  let listsInStorage = localStorage.getItem('listInStorage')
  if (listsInStorage == null) {
    listsObj = []
  } else {
    listsObj = JSON.parse(listsInStorage)
  }
  let newIndex = listsObj.findIndex(item => item.id === index)
  listsObj.splice(newIndex, 1)
  localStorage.setItem('listInStorage', JSON.stringify(listsObj))
  showLists()
}

// On CheckMark Function

function checkAddress (target) {
  let classes = target.parentElement
  if (target.checked) {
    classes.classList.toggle('completed')

    let listsInStorage = localStorage.getItem('listInStorage')

    if (listsInStorage == null) {
      listsObj = []
    } else {
      listsObj = JSON.parse(listsInStorage)
    }
    listsObj.find((o, i) => {
      if (o.id === target.id) {
        listsObj[i]['isCompleted'] = true
        return true
      }
    })
    localStorage.setItem('listInStorage', JSON.stringify(listsObj))
  } else {
    classes.classList.remove('completed')
    listsObj.find((o, i) => {
      if (o.id === target.id) {
        listsObj[i]['isCompleted'] = false
        return true
      }
    })
    localStorage.setItem('listInStorage', JSON.stringify(listsObj))
  }
}
function create_UUID () {
  var dt = new Date().getTime()
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (
    c
  ) {
    var r = (dt + Math.random() * 16) % 16 | 0
    dt = Math.floor(dt / 16)
    return (c == 'x' ? r : (r & 0x3) | 0x8).toString(16)
  })
  return uuid
}
