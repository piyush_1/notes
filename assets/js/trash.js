// Function to Show Trash Items
function showNotesBin () {
  let notesInStorageBin = localStorage.getItem('notesInStorageBin')
  let noteBinElements = document.getElementById('notesBinSection')
  let htmlBin = ''

  if (notesInStorageBin == null) {
    notesObjBin = []
  } else {
    notesObjBin = JSON.parse(notesInStorageBin)
  }

  notesObjBin.forEach(function (element, index) {
    htmlBin += `
    <div class="card my-3 mx-3 new-side">
        <div class="card-body">
          <p class="card-text">${element}</p>
          <div class="col-sm text-center">
            <button id="${index}" onclick="deleteBinNote(this.id)" class="btn btn-delete"
              ><i class="far fa-trash-alt"></i> Remove Permanently</button
            >
          </div>
        </div>
      </div>    
    `
  })

  if (notesObjBin.length != 0) {
    noteBinElements.innerHTML = htmlBin
  } else {
    noteBinElements.innerHTML = `<p style="margin-right: 10px; padding:20px; text-align: center; font-size: 28px">Trash is Empty</p>`
  }
}

// Function to Delete from Bin

function deleteBinNote (index) {
  let notesInStorageBin = localStorage.getItem('notesInStorageBin')
  if (notesInStorageBin == null) {
    notesObjBin = []
  } else {
    notesObjBin = JSON.parse(notesInStorageBin)
  }
  notesObjBin.splice(index, 1)
  localStorage.setItem('notesInStorageBin', JSON.stringify(notesObjBin))
  showNotesBin()
}
