// Firebase Credentials
var firebaseConfig = {
  apiKey: 'AIzaSyAxBMxJhTWIwUSEbd4_PHiJ4-532GxShms',
  authDomain: 'notesapp00.firebaseapp.com',
  databaseURL: 'https://notesapp00-default-rtdb.firebaseio.com',
  projectId: 'notesapp00',
  storageBucket: 'notesapp00.appspot.com',
  messagingSenderId: '493685402083',
  appId: '1:493685402083:web:20b5811d44f769224098d6'
}
firebase.initializeApp(firebaseConfig)

// Function to get time

function getTime () {
  var today = new Date()
  var date =
    today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate()
  var time =
    today.getHours() +
    '' +
    today.getMinutes() +
    '' +
    today.getSeconds() +
    '' +
    (today.getMilliseconds() / 100).toFixed(0)

  return date + time
}

let addNoteBtn = document.getElementById('addNoteBtn')
let welcomeSection = document.getElementById('welcomeSection')
let saveNoteBtn = document.getElementById('saveNoteBtn')
let cancelSaveBtn = document.getElementById('cancelSaveBtn')
let cancelBtn = document.getElementById('cancelSaveBtn')
let newNoteToAdd = document.getElementById('noteToAdd')
let noteAppClientInfo = localStorage.getItem('noteAppClientName')
let user = noteAppClientInfo.toLowerCase().replace(/['"]+/g, '')
showNotes()

welcomeSection.innerHTML = `<h3> Welcome to notes of ${user}</h3>`

// EventListener to Add Notes

addNoteBtn.addEventListener('click', function (e) {
  e.preventDefault()
  let noteToAdd = document.getElementById('noteToAdd')
  let warning = document.getElementById('invalid-note-warning')
  var regex = /[A-Za-z0-9]/g
  let noteValue = noteToAdd.value
  if (regex.test(noteValue)) {
    var newNoteItem = {
      id: create_UUID(),
      name: noteValue,
      createdAt: getTime()
    }
    firebase
      .database()
      .ref('users/' + user + '/notes/' + newNoteItem.createdAt)
      .set({
        uId: newNoteItem.id,
        data: newNoteItem.name,
        createdAt: newNoteItem.createdAt
      })
  } else {
    warning.style.visibility = 'visible'
    setTimeout(warningRemove, 5000)
  }

  showNotes()
  noteToAdd.value = ''
})
function warningRemove () {
  let warning = document.getElementById('invalid-note-warning')
  warning.style.visibility = 'hidden'
}

//Function to Delete Notes

function deleteNotes (index) {
  firebase
    .database()
    .ref('users/' + user + '/notes/' + index)
    .remove()
  showNotes()
}

// Function To Show Notes

function showNotes () {
  let noteElements = document.getElementById('notesSection')
  let notesHtml = ''
  let db = firebase.database().ref('users/' + user + '/notes/')
  db.on('child_added', function (data) {
    var note = data.val()
    notesHtml += `
    <div class="card my-3 mx-3">
        <div class="card-body">
          <p class="card-text">${note.data}</p>
          <div class="col-sm text-center">
            <button id="${note.createdAt}" title="Edit Note" onclick="editNotes(this.id, this)" class="btn btn-edit"
              ><i class="fas fa-pen"></i></button
            >
            <button id="${note.createdAt}" title="Delete Note" onclick="deleteNotes(this.id)" class="btn btn-delete"
              ><i class="far fa-trash-alt"></i></button
            >
          </div>
        </div>
      </div> 
  `
    noteElements.innerHTML = notesHtml
    /*if (notesHtml == null) {
      console.log('nhi aaya')
      noteElements.innerHTML = `<p style="margin-right: 10px; text-align: center; font-size: 20px">Nothing to Show</p>`
    } else {
      console.log('aaya')
    }*/
  })
}
// Function to Edit Notes

function editNotes (index, e) {
  let fetchValue = e.parentElement.parentElement
  let currentTextValue = fetchValue.getElementsByClassName('card-text')[0]
    .outerText
  noteToAdd.value = currentTextValue
  localStorage.setItem('noteToEdit', JSON.stringify(index))
  saveNoteBtn.style.visibility = 'visible'
  cancelSaveBtn.style.visibility = 'visible'
  addNoteBtn.style.visibility = 'hidden'
  let indexParent = e.parentElement.parentElement.parentElement
  let deletebtn = document.getElementsByClassName('btn-delete')
  let editbtn = document.getElementsByClassName('btn-edit')

  for (let i = 0; i < deletebtn.length; i++) {
    deletebtn[i].disabled = true
  }
  for (let i = 0; i < editbtn.length; i++) {
    editbtn[i].disabled = true
  }
  indexParent.classList.toggle('selected')
}

saveNoteBtn.addEventListener('click', function (target) {
  target.preventDefault()
  let index = localStorage.getItem('noteToEdit').replace(/['"]+/g, '')
  let newNoteToAdd = document.getElementById('noteToAdd')
  let warning = document.getElementById('invalid-note-warning')
  var regex = /[A-Za-z0-9]/g

  console.log(index)
  if (regex.test(newNoteToAdd.value)) {
    let db = firebase.database().ref('users/' + user + '/notes/' + index)

    db.update({ data: newNoteToAdd.value })

    showNotes()
    saveNoteBtn.style.visibility = 'hidden'
    cancelSaveBtn.style.visibility = 'hidden'
    addNoteBtn.style.visibility = 'visible'
  } else {
    warning.style.visibility = 'visible'
    setTimeout(warningRemove, 5000)
  }
})

// Cancel Button Functionality

cancelBtn.addEventListener('click', function (e) {
  e.preventDefault()
  newNoteToAdd.value = ''
  saveNoteBtn.style.visibility = 'hidden'
  cancelSaveBtn.style.visibility = 'hidden'
  addNoteBtn.style.visibility = 'visible'

  let index = document.getElementById('saveIndex')
  let selectedNote = document.getElementsByClassName('selected')

  selectedNote[0].classList.remove('selected')
  let deletebtn = document.getElementsByClassName('btn-delete')
  let editbtn = document.getElementsByClassName('btn-edit')

  for (let i = 0; i < deletebtn.length; i++) {
    deletebtn[i].disabled = false
  }
  for (let i = 0; i < editbtn.length; i++) {
    editbtn[i].disabled = false
  }
})

function create_UUID () {
  var dt = new Date().getTime()
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (
    c
  ) {
    var r = (dt + Math.random() * 16) % 16 | 0
    dt = Math.floor(dt / 16)
    return (c == 'x' ? r : (r & 0x3) | 0x8).toString(16)
  })
  return uuid
}
