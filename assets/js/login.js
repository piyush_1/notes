let addUserBtn = document.getElementById('addUserBtn')
let warning = document.getElementById('warning')
let noteAppClientInfo = localStorage.getItem('noteAppClientName')
addUserBtn.addEventListener('click', function (e) {
  e.preventDefault()
  let userToAdd = document.getElementById('clientUsername')
  let username = userToAdd.value
  userToAdd.value = ''
  var regex = /[A-Za-z]/g
  if (regex.test(username)) {
    userObj = username
    localStorage.setItem('noteAppClientName', JSON.stringify(userObj))
    location.replace('/notes.html')
  } else {
    warning.style.visibility = 'visible'
    warning.style.opacity = 1
    setTimeout(hideWarning, 5000)
  }
})
function hideWarning () {
  warning.style.visibility = 'hidden'
  warning.style.opacity = 0
}
